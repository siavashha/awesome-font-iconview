# Awesome-IconView #

The Awesome-IconView is basically just a TextView, which uses an Icon-Font to show "Flat-Design"-Icons. In this library the [Font-Awesome](http://fortawesome.github.io/Font-Awesome/) is used but you also can create your own Icon-Font.
To create your own Icon-Font you can use for example [Iconmoon](https://icomoon.io/)

### Usage ###

The IconView has two attributes

* icon - to set the icon string
* measureRatio - keeps the view width the same as the height and other way arround

You can find all icons by name in the strings.xml, the Demo-App will show you the Icon and the depending name from the strings.xml

```
#!xml

<com.homami.android.IconView
    android:id="@+id/icon"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:padding="5dp"
    android:textSize="18dp"
    android:gravity="center"
    app:icon="@string/icon_search"
    app:measureRatio="true"/>
```

### Demo ###

![ihr_qr_code_ohne_logo.jpg](https://bitbucket.org/repo/g5L9eG/images/782731058-ihr_qr_code_ohne_logo.jpg)

[Demo](https://play.google.com/store/apps/details?id=com.mirsoft.example)

### Contribution ###

If you would like to contribute to this project make sure you send pull request to dev branch or create an issue.

### Developed by ###

* Mirko Häberlin